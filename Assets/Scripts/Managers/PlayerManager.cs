﻿using UnityEngine;
using Assets.Scripts.Utils;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerManager : NetworkBehaviour
{
    private readonly string RECOVER_FN = "ResetMovementSpeed";
    private readonly string ANIM_SPEED_PARAM = "Speed";
    private readonly string ANIM_GROUNDED_PARAM = "IsGrounded";
    private readonly string GROUND_CHECK_CHILD_NAME = "Ground Check";

    public AudioClip JumpSound;
    public float MovementSpeedMultiplier = 10.0f;
    public float JumpSpeedMultiplier = 11.5f;
    public LayerMask GroundLayer;
    public float HitBlinkDuration = 3.0f;
    public float HitBlinkFrequency = 0.2f;
    public float HitSlowDuration = 3.0f;
    public Sprite MarioSprite;
    public Sprite LuigiSprite;
    public RuntimeAnimatorController MarioController;
    public RuntimeAnimatorController LuigiController;

    private uint PlayerIndex;
    [SyncVar]
    private int PlayerCoins;

    public int Coins
    {
        get
        {
            return PlayerCoins;
        }

        set
        {
            PlayerCoins = value;
        }
    }
    public bool AcceptInput { get; set; }
    public uint Index
    {
        get
        {
            return PlayerIndex;
        }
    }

    public bool IsGrounded
    {
        get
        {
            return Physics2D.OverlapBox(GroundCheck.position, new Vector2(GroundCheck.localScale.x, GroundCheck.localScale.y), 0.0f, GroundLayer);
        }
    }

    private Animator PlayerAnimator;
    private Transform GroundCheck;
    private bool HasJumped = false;
    private bool CanJumpFromMonsterCollision = true;
    private float OnHitSpeedMultiplier = 1.0f;

    private void SetCharacterLook(Sprite Sprite, RuntimeAnimatorController RuntimeController)
    {
        SpriteRenderer Renderer = GetComponent<SpriteRenderer>();
        if (Renderer != null)
        {
            Renderer.sprite = Sprite;
        }

        if (PlayerAnimator != null)
        {
            PlayerAnimator.runtimeAnimatorController = RuntimeController;
        }
    }

    private void SetMarioLook()
    {
        SetCharacterLook(MarioSprite, MarioController);
    }

    private void SetLuigiLook()
    {
        SetCharacterLook(LuigiSprite, LuigiController);
        SpawnManager Spawn = GameplayStatics.Spawn;
        if (Spawn)
        {
            Spawn.CanStartGame = true;
        }
    }

    private void PickCharacterLook()
    {
        if (PlayerIndex % 2 == 0)
        {
            SetMarioLook();
        }
        else
        {
            SetLuigiLook();
        }
    }

    private void InitCameraPosition()
    {
        if (isLocalPlayer && Camera.main)
        {
            CameraMovement Movement = Camera.main.GetComponent<CameraMovement>();
            if (Movement)
            {
                Movement.SetPlayer(gameObject.transform);
            }
            else
            {
                Debug.Log("No camera movement script");
            }
        }
        else
        {
            Debug.Log("No main camera");
        }
    }

    private void AssignPlayerIndex()
    {
        PlayerIndex = GameplayStatics.Spawn.PlayerCount++;
    }

    private void Start()
    {
        AcceptInput = true;
        AssignPlayerIndex();
        PlayerAnimator = GetComponent<Animator>();
        GroundCheck = transform.FindChild(GROUND_CHECK_CHILD_NAME);
        PickCharacterLook();
    }

    public override void OnStartLocalPlayer()
    {
        GameplayStatics.LocalPlayerIndex = netId.Value;
        InitCameraPosition();
    }

    private void Awake()
    {
        if (GameplayStatics.Audio)
        {
            GameplayStatics.Audio.RestartMusic();
        }
    }

    private IEnumerator DoBlink(float Duration, float BlinkTime)
    {
        float StartTime = 0.0f;
        Renderer PlayerRenderer = GetComponent<Renderer>();
        while (Duration > 0.0f)
        {
            PlayerRenderer.enabled = !PlayerRenderer.enabled;

            yield return new WaitForSeconds(BlinkTime);
            Duration -= BlinkTime;
            StartTime += BlinkTime;
        }

        PlayerRenderer.enabled = true;
    }

    public void DamagePlayer()
    {
        OnHitSpeedMultiplier = 0.5f;

        StartCoroutine(DoBlink(HitBlinkDuration, HitBlinkFrequency));

        CancelInvoke(RECOVER_FN);
        Invoke(RECOVER_FN, HitSlowDuration);
    }

    private void ResetMovementSpeed()
    {
        OnHitSpeedMultiplier = 1.0f;
    }

    private void OnCollisionEnter2D(Collision2D Other)
    {
        GameObject OtherGO = Other.gameObject;
        if (OtherGO.tag.Equals("Monster") || OtherGO.tag.Equals("Player"))
        {
            if (CanJumpFromMonsterCollision && !IsGrounded)
            {
                Jump();
                CanJumpFromMonsterCollision = false;
            }
        }
    }

    private void OnCollisionExit2D(Collision2D Other)
    {
        GameObject OtherGO = Other.gameObject;
        if (OtherGO.tag.Equals("Monster") || OtherGO.tag.Equals("Player"))
        {
            CanJumpFromMonsterCollision = true;
        }
    }

    private void ApplyWalkAnimation(float HorizontalSpeed)
    {
        if (HorizontalSpeed > 0.0f)
        {
            transform.localScale = MyConstants.NEGATIVE_HORIZONTAL_SCALE;
        }
        else if (HorizontalSpeed < 0.0f)
        {
            transform.localScale = Vector3.one;
        }

        PlayerAnimator.SetFloat(ANIM_SPEED_PARAM, Mathf.Abs(HorizontalSpeed));
    }

    public void Jump()
    {
        Rigidbody2D PlayerRigidBody = GetComponent<Rigidbody2D>();
        if (PlayerRigidBody != null)
        {
            PlayerRigidBody.AddForce(Vector2.up * JumpSpeedMultiplier, ForceMode2D.Impulse);
        }
    }

    private void TryJump()
    {
        if (IsGrounded)
        {
            AudioManager Audio = GameplayStatics.Audio;
            if (Audio)
            {
                Audio.PlayEffect(JumpSound);
            }

            Jump();
            HasJumped = true;
        }
    }

    private void ApplyHorizontalMovement(float HorizontalSpeed)
    {
        Rigidbody2D PlayerRigidBody = GetComponent<Rigidbody2D>();
        if (PlayerRigidBody == null)
        {
            return;
        }

        PlayerRigidBody.velocity = new Vector2(Mathf.Lerp(PlayerRigidBody.velocity.x, OnHitSpeedMultiplier * HorizontalSpeed * MovementSpeedMultiplier, Time.fixedDeltaTime * MovementSpeedMultiplier / 2), PlayerRigidBody.velocity.y);
    }

    private void CheckHorizontalMovement()
    {
        float HorizontalSpeed = Input.GetAxis("Horizontal");
        ApplyWalkAnimation(HorizontalSpeed);

        ApplyHorizontalMovement(HorizontalSpeed);
    }

    private void CheckVerticalMovement()
    {
        if (Input.GetButton("Jump"))
        {
            if (!HasJumped)
            {
                TryJump();
            }
        }
        else if (IsGrounded)
        {
            HasJumped = false;
        }

        PlayerAnimator.SetBool(ANIM_GROUNDED_PARAM, IsGrounded);
    }

    private void OnGUI()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        GUI.Box(new Rect(Screen.width - 200.0f, Screen.height - 35.0f, 200.0f, 80.0f), "Player " + (PlayerIndex > 0 ? "2" : "1") + " | Coins: " + Coins);
    }

    // Physics update
    void FixedUpdate()
    {
        if (!isLocalPlayer || !AcceptInput)
        {
            return;
        }

        CheckHorizontalMovement();
        CheckVerticalMovement();
    }
}
