﻿using UnityEngine;

public class CoinManager : MonoBehaviour 
{
    public AudioClip CoinPickupSound;
    private bool HasBeenPickedUp = false;

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            PlayerManager Manager = collision.gameObject.GetComponent<PlayerManager>();
            if (Manager != null && !HasBeenPickedUp)
            {
                Manager.Coins += 1;
                HasBeenPickedUp = true;
            }
            AudioManager Audio = GameplayStatics.Audio;
            if (Audio)
            {
                Audio.PlayEffect(CoinPickupSound);
            }

            Destroy(gameObject);
        }
    }
}
