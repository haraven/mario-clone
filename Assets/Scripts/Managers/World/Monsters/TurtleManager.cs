﻿using UnityEngine;
using Assets.Scripts.Utils;

public class TurtleManager : MonoBehaviour
{
    private readonly string RECOVERY_FN = "TryRecover";
    private readonly string ANIM_FLIPPED_PARAM = "IsFlipped";
    private readonly string ANIM_SPEED_PARAM = "Speed";

    public AudioClip TurtleKickedSound;
    public float MovementSpeed = 1.0f;
    public float TimeBeforeRecovery = 5.0f;
    public float LaunchSpeed = 300.0f;

    private Animator TurtleAnimator;
    private Rigidbody2D TurtleRigidBody;
    private bool CanRecover = true;
    private bool WasLaunched = false;
    private GameObject WhoLaunched;

    public bool IsFlipped 
    {
        get
        {
            return TurtleAnimator.GetBool(ANIM_FLIPPED_PARAM);
        }
    }


    private void Start()
    {
        TurtleAnimator = GetComponent<Animator>();
        TurtleRigidBody = GetComponent<Rigidbody2D>();
    }

    private void Flip()
    {
        if (TurtleRigidBody)
        {
            TurtleRigidBody.velocity = new Vector3(0.0f, TurtleRigidBody.velocity.y);
        }

        TurtleAnimator.SetBool(ANIM_FLIPPED_PARAM, true);
    }

    private void Launch(bool LaunchLeft, GameObject Who)
    {
        TurtleRigidBody.AddForce((LaunchLeft ? Vector2.left : Vector2.right) * LaunchSpeed, ForceMode2D.Impulse);
        WhoLaunched = Who;
    }

    private void DoPlayerDamage(GameObject PlayerGO)
    {
        PlayerManager Manager = PlayerGO.GetComponent<PlayerManager>();
        if (Manager != null)
        {
            if (Manager.IsGrounded)
            {
                Manager.DamagePlayer();
            }
        }
    }

    private void OnPlayerCollision (GameObject PlayerGO)
    {
        if (TurtleAnimator.GetBool(ANIM_FLIPPED_PARAM) == true)
        {
            if (!WasLaunched)
            {
                bool IsLookingLeft = PlayerGO.transform.localScale.x > 0.0f;
                AudioManager Audio = GameplayStatics.Audio;
                if (Audio)
                {
                    Audio.PlayEffect(TurtleKickedSound);
                }

                Launch(IsLookingLeft, PlayerGO);
                WasLaunched = true;
            }
            else if (WhoLaunched != PlayerGO)
            {
                DoPlayerDamage(PlayerGO);
            }
            CancelInvoke();
        }
        else
        {
            DoPlayerDamage(PlayerGO);
            Flip();
        }

        CanRecover = false;
        CancelInvoke();
        Invoke(RECOVERY_FN, TimeBeforeRecovery);
    }

    private void OnMonsterCollision(GameObject OtherGO)
    {
        if (!WasLaunched)
        {
            bool IsLookingLeft = OtherGO.transform.localScale.x > 0.0f;
            Launch(IsLookingLeft, OtherGO);
            WasLaunched = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D Other)
    {
        GameObject OtherGO = Other.gameObject;
        if (OtherGO.tag.Equals("Player"))
        {
            OnPlayerCollision(OtherGO);
        }
        else if (OtherGO.tag.Equals("Monster"))
        {
            OnMonsterCollision(OtherGO);
        }
    }

    private void OnCollisionExit2D(Collision2D Other)
    {
        if (Other.gameObject.tag.Equals("Player"))
        {
            CanRecover = true;
        }
    }

    private void TryRecover()
    {
        if (TurtleRigidBody)
        {
            if (CanRecover && TurtleRigidBody.velocity.x == 0)
            {
                TurtleAnimator.SetBool(ANIM_FLIPPED_PARAM, false);
            }
            else
            {
                Invoke(RECOVERY_FN, TimeBeforeRecovery);
            }
        }
    }

    private GameObject FindPlayer()
    {
        GameObject[] GOs;
        GOs = GameObject.FindGameObjectsWithTag("Player");
        GameObject Closest = null;
        float Distance = Mathf.Infinity;
        Vector3 Position = transform.position;
        foreach (GameObject GO in GOs)
        {
            Vector3 Diff = GO.transform.position - Position;
            float CurrentDistance = Diff.sqrMagnitude;
            if (CurrentDistance < Distance)
            {
                Closest = GO;
                Distance = CurrentDistance;
            }
        }

        return Closest;
    }

    private void StopMoving()
    {
        TurtleRigidBody.velocity = new Vector2(0.0f, TurtleRigidBody.velocity.y);
        TurtleAnimator.SetFloat(ANIM_SPEED_PARAM, 0.0f);
    }

    private void TryMoveTowards(GameObject Obj)
    {
        if (!TurtleAnimator.GetBool(ANIM_FLIPPED_PARAM))
        {
            Vector3 RelativeDistance = Obj.transform.position - transform.position;
            if (RelativeDistance.x > 0.001f || RelativeDistance.x < -0.001f)
            {
                if (RelativeDistance.x < 0.0f)
                {
                    transform.localScale = MyConstants.NEGATIVE_HORIZONTAL_SCALE;
                }
                else
                {
                    transform.localScale = Vector3.one;
                }

                if (TurtleRigidBody)
                {
                    TurtleRigidBody.velocity = new Vector2(Mathf.Lerp(TurtleRigidBody.velocity.x, (RelativeDistance.x > 0.0 ? 1.0f : -1.0f) * MovementSpeed, Time.fixedDeltaTime * MovementSpeed / 2), TurtleRigidBody.velocity.y);
                    TurtleAnimator.SetFloat(ANIM_SPEED_PARAM, 1.0f);
                }
            }
            else
            {
                StopMoving();
            }
        }
        else
        {
            StopMoving();
        }
    }

    private void FixedUpdate()
    {
        if (TurtleRigidBody.velocity.x == 0.0f)
        {
            WhoLaunched = null;
            WasLaunched = false;
        }
        if (!TurtleAnimator.GetBool(ANIM_FLIPPED_PARAM))
        {
            GameObject NearestPlayer = FindPlayer();
            if (NearestPlayer)
            {
                TryMoveTowards(NearestPlayer);
            }
            else
            {
                StopMoving();
            }
        }
    }
}
