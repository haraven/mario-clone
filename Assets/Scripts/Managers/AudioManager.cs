﻿using UnityEngine;

public class AudioManager : MonoBehaviour
{
    private static readonly string INIT_SOURCES_FN = "InitSources";

    private AudioSource SoundEffectsSource;
    private AudioSource MusicSource;

    private void Awake()
    {
        InvokeRepeating(INIT_SOURCES_FN, 0.1f, 0.1f);
    }

    private void InitSources()
    {
        SoundEffectsSource = GameObject.FindGameObjectWithTag("SoundEffects").GetComponent<AudioSource>();
        MusicSource = GameObject.FindGameObjectWithTag("Music").GetComponent<AudioSource>();
        InitVolume();
        CancelInvoke(INIT_SOURCES_FN);
    }

    private void InitVolume()
    {
        PlayerSettings Settings = new PlayerSettings();
        Settings.Load();
        InitFromSettings(Settings);
    }

    public void InitFromSettings(PlayerSettings Settings)
    {
        SoundEffectsVolume = Settings.SoundEffectsVolume;
        MusicVolume = Settings.MusicVolume;
    }

    private void OnServerInitialized()
    {
        InitVolume();
    }

    private void OnConnectedToServer()
    {
        InitVolume();
    }

    public void PlayEffect(AudioClip Clip)
    {
        if (Clip != null && SoundEffectsSource != null)
        {
            SoundEffectsSource.PlayOneShot(Clip);
        }
        else
        {
            Debug.Log("Could not play sound effect.");
        }
    }

    public void PlayMusic(AudioClip Clip)
    {
        if (Clip != null && MusicSource != null)
        {
            if (MusicSource.isPlaying)
            {
                MusicSource.Stop();
                MusicSource.PlayOneShot(Clip);
            }
        }
        else
        {
            Debug.Log("Could not play music.");
        }
    }

    public void RestartMusic()
    {
        AudioClip MusicClip = MusicSource.clip;
        MusicSource.Stop();
        MusicSource.PlayOneShot(MusicClip);
        MusicSource.loop = true;
    }

    public float SoundEffectsVolume
    {
        get
        {
            if (SoundEffectsSource)
            {
                return SoundEffectsSource.volume;
            }
            else
            {
                return PlayerSettings.SOUND_EFFECTS_VOLUME_DEFAULT;
            }
        }
        set
        {
            if (SoundEffectsSource != null)
            {
                SoundEffectsSource.volume = Mathf.Clamp(value, 0.0f, 1.0f);
            }
        }
    }

    public float MusicVolume
    {
        get
        {
            if (MusicSource)
            {
                return MusicSource.volume;
            }
            else
            {
                return PlayerSettings.MUSIC_VOLUME_DEFAULT;
            }
        }
        set
        {
            if (MusicSource != null)
            {
                MusicSource.volume = Mathf.Clamp(value, 0.0f, 1.0f);
            }
        }
    }
}
