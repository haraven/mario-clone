﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum MenuType : byte
{
    MENU_MAIN = 0,
    MENU_OPTIONS,
    MENU_PAUSE,
    MENU_GAME_OVER,
}

public class GameManager : MonoBehaviour
{
    private readonly int MainSceneIndex = 1;
    private readonly GUI.WindowFunction[] MenuFunctions = null;
    private readonly string[] MenuNames = null;

    public AudioClip ButtonClickSound;
    public bool ShowMenuOnAwake = true;

    public int MenuWidth = 400;
    public int MenuHeight = 300;

    private PlayerSettings Settings = new PlayerSettings();

    public PlayerSettings PlayerSettings
    {
        get
        {
            return Settings;
        }
    }
    public bool IsMenuActive { get; set; }
    public MenuType ActiveMenu { get; set; }
    public bool IsGameOver
    {
        get
        {
            return ActiveMenu == MenuType.MENU_GAME_OVER;
        }
    }

    public GameManager()
    {
        GameplayStatics.LocalPlayerIndex = 0xFFFFFFFF;

        MenuFunctions = new GUI.WindowFunction[]
        {
            ShowMainMenu,
            ShowOptionsMenu,
            ShowPauseMenu,
            ShowGameOverMenu
        };

        MenuNames = new string[]
        {
            "Main Menu",
            "Options",
            "Paused",
            "Game Over!"
        };
    }

    public void LoadSceneByIndex(int SceneIndex)
    {
        SceneManager.LoadSceneAsync(SceneIndex);
    }

    private void Awake()
    {
        IsMenuActive = ShowMenuOnAwake;
        ActiveMenu = MenuType.MENU_MAIN;

        //Application.runInBackground = true;
        //DontDestroyOnLoad(transform.parent.gameObject); // persist the game manager(s)

        Settings.Load();
        GameplayStatics.Audio.InitFromSettings(Settings);

        //Countdown = GameplayStatics.Countdown;
        //GameObject CountdownGO = GameObject.FindGameObjectWithTag("Countdown");
        //if (CountdownGO)
        //{
        //    CountdownText = CountdownGO.GetComponent<Text>();
        //}
    }

    private void OnGUI()
    {
        if (IsMenuActive)
        {
            Rect MenuLocation = new Rect((Screen.width - MenuWidth) / 2, (Screen.height - MenuHeight) / 2, MenuWidth, MenuHeight);
            GUILayout.Window(0, MenuLocation, MenuFunctions[(byte)ActiveMenu], MenuNames[(byte)ActiveMenu]);
        }
    }

    private void OnButtonClick()
    {
        if (GameplayStatics.Audio)
            GameplayStatics.Audio.PlayEffect(ButtonClickSound);
    }

    private void DrawSliders()
    {
        if (GameplayStatics.Audio)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Music volume: ", GUILayout.Width(90.0f));
            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label(Settings.MusicVolume.ToString("0.00"));
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GameplayStatics.Audio.MusicVolume = Settings.MusicVolume = GUILayout.HorizontalSlider(Settings.MusicVolume, 0.0f, 1.0f);
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Effects volume: ", GUILayout.Width(90.0f));
            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label(Settings.SoundEffectsVolume.ToString("0.00"));
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GameplayStatics.Audio.SoundEffectsVolume = Settings.SoundEffectsVolume = GUILayout.HorizontalSlider(Settings.SoundEffectsVolume, 0.0f, 1.0f);
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        }
    }

    private void ShowMainMenu(int ID)
    {
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.Label("Highscore: " + Settings.HighScore, GUILayout.Width(90.0f));
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        if (GUILayout.Button("Start"))
        {
            OnButtonClick();
            IsMenuActive = false;
            LoadSceneByIndex(MainSceneIndex);
        }
        if (GUILayout.Button("Options"))
        {
            OnButtonClick();
            ActiveMenu = MenuType.MENU_OPTIONS;
        }
        if (!Application.isWebPlayer && !Application.isEditor) // can't exit in a web player or in the editor
        {
            if (GUILayout.Button("Exit"))
            {
                OnButtonClick();
                Application.Quit();
            }
        }
    }

    private void ShowOptionsMenu(int ID)
    {
        DrawSliders();

        if (GUILayout.Button("Back"))
        {
            OnButtonClick();
            Settings.Save();
            ActiveMenu = MenuType.MENU_MAIN;
        }
    }

    private void ShowPauseMenu(int ID)
    {

    }

    private void ShowGameOverMenu(int ID)
    {
        GameObject[] Players = GameObject.FindGameObjectsWithTag("Player");
        PlayerManager LeaderManager = null;
        bool IsTie = true;
        foreach (GameObject Player in Players)
        {
            PlayerManager Manager = Player.GetComponent<PlayerManager>();
            if (Manager)
            {
                int PlayerPoints = Manager.Coins;
                if (LeaderManager == null || PlayerPoints > LeaderManager.Coins)
                {
                    LeaderManager = Manager;
                    IsTie = false;
                }
                else if (Manager != null && PlayerPoints == LeaderManager.Coins)
                {
                    IsTie = true;
                }
            }
        }
        GUILayout.BeginVertical();
        if (LeaderManager == null)
        {
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("Unknown error!");
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }
        else
        {
            if (IsTie)
            {
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                GUILayout.Label("The match ended in a tie! Everyone's a winner! Coins gathered: " + LeaderManager.Coins);
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
            }
            else
            {
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                GUILayout.Label("Player " + (LeaderManager.Index > 0 ? "2" : "1") + " won, with " + LeaderManager.Coins + (LeaderManager.Coins > 1 ? " coins!" : " coin!"));
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
            }

            if (Settings.HighScore < LeaderManager.Coins)
            {
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                GUILayout.Label("New highscore was set!");
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
                Settings.HighScore = LeaderManager.Coins;
                Settings.Save();
            }
        }

        if (GUILayout.Button("Back to main menu"))
        {
            SpawnManager Spawns = GameplayStatics.Spawn;
            if (Spawns)
            {
                Spawns.ResetCounts();
            }

            LoadSceneByIndex(0);
        }
        GUILayout.EndVertical();
    }

    private void DisablePlayers()
    {
        GameObject[] Players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject Player in Players)
        {
            PlayerManager Manager = Player.GetComponent<PlayerManager>();
            if (Manager)
            {
                Manager.AcceptInput = false;
            }
        }
    }

    private void DestroyList(GameObject[] GOs)
    {
        foreach (GameObject GO in GOs)
        {
            Destroy(GO);
        }
    }

    private void DestroyAllWorldObjects()
    {
        GameObject[] Monsters = GameObject.FindGameObjectsWithTag("Monster");
        DestroyList(Monsters);
        GameObject[] Coins = GameObject.FindGameObjectsWithTag("Coin");
        DestroyList(Coins);
    }

    private void StopSpawning()
    {
        GameObject[] SpawnerGOs = GameObject.FindGameObjectsWithTag("Spawner");
        foreach (GameObject SpawnerGO in SpawnerGOs)
        {
            UnitSpawner Spawner = SpawnerGO.GetComponent<UnitSpawner>();
            if (Spawner)
            {
                Spawner.CanSpawn = false;
            }
        }
    }

    public void StopGame()
    {
        StopSpawning();
        DestroyAllWorldObjects();
        DisablePlayers();

        IsMenuActive = true;
        ActiveMenu = MenuType.MENU_GAME_OVER;
    }

    //private void Update()
    //{
    //    if (!IsMenuActive && Countdown && CountdownText)
    //    {
    //        CountdownText.text = Mathf.RoundToInt(Countdown.Remaining).ToString();
    //        if (Countdown.Remaining <= 0.02 && Countdown.Remaining >= -0.02f)
    //        {
    //            StopGame();
    //        }
    //    }
    //}
}
