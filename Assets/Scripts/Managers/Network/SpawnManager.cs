﻿using UnityEngine.Networking;

public class SpawnManager : NetworkBehaviour 
{
    public uint InitialCoinSpawnCount = 9999;
    public uint InitialTurtleSpawnCount = 5;
    public uint InitialPlayerCount = 0;

    [SyncVar]
    public uint CoinSpawnCount = 9999;
    [SyncVar]
    public uint TurtleSpawnCount = 5;
    [SyncVar]
    public uint PlayerCount = 0;
    [SyncVar]
    public bool CanStartGame = false;

    private void Start()
    {
        ResetCounts();
    }

    public void ResetCounts()
    {
        CoinSpawnCount = InitialCoinSpawnCount;
        TurtleSpawnCount = InitialTurtleSpawnCount;
        PlayerCount = InitialPlayerCount;
    }
}
