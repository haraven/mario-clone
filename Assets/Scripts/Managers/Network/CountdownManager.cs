﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class CountdownManager : NetworkBehaviour
{
    public float TotalTime = 60.0f;

    [SyncVar]
    private float TimeLeft = 0.0f;

    private Text CountdownText;
    private SpawnManager Spawns;
    private GameManager GameMode;

    public float Remaining
    {
        get
        {
            return TimeLeft;
        }
    }

    private void Awake()
    {
        TimeLeft = TotalTime;
        CountdownText = GameObject.FindGameObjectWithTag("Countdown").GetComponent<Text>();
        if (CountdownText)
        {
            CountdownText.text = Mathf.RoundToInt(TimeLeft).ToString();
        }

        Spawns = GameplayStatics.Spawn;
        GameMode = GameplayStatics.GameMode;
    }

    void Update()
    {
        if (GameMode && GameMode.IsGameOver)
        {
            return;
        }
        else if (GameMode && Spawns && Spawns.CanStartGame)
        {
            CountdownText.text = Mathf.RoundToInt(TimeLeft).ToString();
            if (TimeLeft <= 0.02f && TimeLeft >= -0.02f)
            {
                GameMode.StopGame();
                return;
            }

            TimeLeft -= Time.deltaTime;
        }
    }
}
