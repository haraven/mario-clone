﻿using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    private static readonly string IMAGE_OVERLAY_NAME = "GameOverlay";

    public Transform PlayerTransform;
    public float SmoothTime = 0.3f;
    private Vector3 Velocity = Vector3.zero;

    private GameObject ImageOverlay;

    private void Awake()
    {
        transform.rotation = Quaternion.Euler(30.0f, 0.0f, 0.0f);
        ImageOverlay = GameObject.FindGameObjectWithTag(IMAGE_OVERLAY_NAME);
    }

    void FixedUpdate()
    {
        if (PlayerTransform != null)
        {
            if (ImageOverlay)
            {
                ImageOverlay.SetActive(false);
            }

            transform.position = Vector3.SmoothDamp(transform.position, PlayerTransform.position, ref Velocity, SmoothTime);
        }
        else if (ImageOverlay)
        {
            ImageOverlay.SetActive(true);
        }
	}

    public void SetPlayer(Transform target)
    {
        PlayerTransform = target;
    }
}
