﻿using UnityEngine;
using UnityEngine.Networking;

public class UnitSpawner : NetworkBehaviour
{
    public GameObject CoinPrefab;
    public GameObject TurtlePrefab;
    public float SpawnRate = 5.0f;
    public float LaunchSpeed = 300.0f;

    public bool CanSpawn { get; set; }

    private SpawnManager Manager;

    private void Start()
    {
        CanSpawn = true;
    }

    public override void OnStartServer()
    {
        Manager = GameplayStatics.Spawn;
    }

    private float TimeElapsed = 0.0f;

    private void Update()
    {
        if (CanSpawn)
        {
            if (Manager)
            {
                if (Manager.PlayerCount >= 2)
                {
                    float Modulus = TimeElapsed % SpawnRate;
                    if (Modulus != 0.0f && Modulus <= 0.02f && Modulus >= -0.02f)
                    {
                        GameObject Unit = null;
                        int SpawnTurtle = Random.Range(0, 2);
                        if (SpawnTurtle > 0 && Manager.TurtleSpawnCount > 0)
                        {
                            Unit = Instantiate(TurtlePrefab, transform.position, transform.rotation);
                            --Manager.TurtleSpawnCount;
                        }
                        else if (Manager.CoinSpawnCount > 0)
                        {
                            Unit = Instantiate(CoinPrefab, transform.position, transform.rotation);
                            --Manager.CoinSpawnCount;
                        }

                        if (Unit != null)
                        {
                            NetworkServer.Spawn(Unit);
                            Unit.GetComponent<Rigidbody2D>().velocity = Random.onUnitSphere * LaunchSpeed;
                        }
                    }
                    TimeElapsed += Time.deltaTime;
                }
            }
            else
            {
                Debug.Log("No spawn manager found. Destroying unit spawner.");
                Destroy(gameObject);
            }
        }
    }
}
