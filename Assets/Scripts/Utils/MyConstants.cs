﻿using UnityEngine;

namespace Assets.Scripts.Utils
{
    public class MyConstants
    {
        public static readonly Vector3 NEGATIVE_HORIZONTAL_SCALE = new Vector3(-1.0f, 1.0f, 1.0f);
    }
}
