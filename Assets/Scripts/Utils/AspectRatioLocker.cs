﻿using UnityEngine;

public class AspectRatioLocker : MonoBehaviour
{
    [Range(1.0f, 20.0f)]
    public float WidthAspect = 16.0f;
    [Range(1.0f, 20.0f)]
    public float HeightAspect = 9.0f;

    private int ScreenWidth = 0;
    private int ScreenHeight = 0;
    private float AspectRatio = 0.0f;

    void Start()
    {
        Resize();
    }

    void Update()
    {
        if ((ScreenHeight != Screen.height) || (ScreenWidth != Screen.width) ||
            (AspectRatio != (WidthAspect / HeightAspect)))
        {
            Resize();
        }
    }

    private void Resize()
    {
        ScreenWidth = Screen.width;
        ScreenHeight = Screen.height;
        AspectRatio = WidthAspect / HeightAspect;

        float WindowAspect = ScreenWidth / (float)ScreenHeight;

        float ScaleHeight = WindowAspect / AspectRatio;
        Camera CameraComp = GetComponent<Camera>();
        Rect CameraRect = CameraComp.rect;

        if (ScaleHeight < 1.0f)
        {
            // Letterbox
            CameraRect.width = 1.0f;
            CameraRect.height = ScaleHeight;
            CameraRect.x = 0;
            CameraRect.y = (1.0f - ScaleHeight) / 2.0f;
            CameraComp.rect = CameraRect;
        }
        else
        {
            // Pillarbox
            float ScaleWidth = 1.0f / ScaleHeight;

            CameraRect.width = ScaleWidth;
            CameraRect.height = 1.0f;
            CameraRect.x = (1.0f - ScaleWidth) / 2.0f;
            CameraRect.y = 0;
        }

        CameraComp.rect = CameraRect;
    }
}
