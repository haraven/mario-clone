﻿using UnityEngine;

public class GameplayStatics : MonoBehaviour
{
    public static uint LocalPlayerIndex { get; set; }

    public static GameManager GameMode
    {
        get
        {
            GameObject Manager = GameObject.FindGameObjectWithTag("GameManager");
            if (Manager != null)
            {
                return Manager.GetComponent<GameManager>();
            }

            return null;
        }
    }

    public static AudioManager Audio
    {
        get
        {
            GameObject Manager = GameObject.FindGameObjectWithTag("AudioManager");
            if (Manager != null)
            {
                return Manager.GetComponent<AudioManager>();
            }

            return null;
        }
    }

    public static SpawnManager Spawn
    {
        get
        {
            GameObject Manager = GameObject.FindGameObjectWithTag("SpawnManager");
            if (Manager != null)
            {
                return Manager.GetComponent<SpawnManager>();
            }

            return null;
        }
    }

    public static CountdownManager Countdown
    {
        get
        {
            GameObject Manager = GameObject.FindGameObjectWithTag("CountdownManager");
            if (Manager != null)
            {
                return Manager.GetComponent<CountdownManager>();
            }

            return null;
        }
    }
}
