﻿using UnityEngine;

public class PlayerSettings
{
    private static readonly string SOUND_EFFECTS_VOLUME_SETTING = "SoundEffectsVolume";
    private static readonly string MUSIC_VOLUME_SETTING = "MusicVolume";
    private static readonly string HIGHSCORE_SETTING = "HighScore";

    public static readonly float SOUND_EFFECTS_VOLUME_DEFAULT = 1.0f;
    public static readonly float MUSIC_VOLUME_DEFAULT = 0.7f;
    public static readonly int HIGHSCORE_DEFAULT = 0;

    public int HighScore { get; set; }
    public float SoundEffectsVolume { get; set; }
    public float MusicVolume { get; set; }

    public void Load()
    {
        SoundEffectsVolume = PlayerPrefs.GetFloat(SOUND_EFFECTS_VOLUME_SETTING, SOUND_EFFECTS_VOLUME_DEFAULT);
        MusicVolume = PlayerPrefs.GetFloat(MUSIC_VOLUME_SETTING, MUSIC_VOLUME_DEFAULT);

        HighScore = PlayerPrefs.GetInt(HIGHSCORE_SETTING, HIGHSCORE_DEFAULT);
    }

    public void Save()
    {
        PlayerPrefs.SetFloat(SOUND_EFFECTS_VOLUME_SETTING, SoundEffectsVolume);
        PlayerPrefs.SetFloat(MUSIC_VOLUME_SETTING, MusicVolume);

        PlayerPrefs.SetInt(HIGHSCORE_SETTING, HighScore);
    }
}
